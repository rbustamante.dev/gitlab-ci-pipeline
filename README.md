# Gitlab CI - Releases

### The purpose of this pipeline is to build and release using semantic git tags.

It includes a build stage where the code is compiled, a version stage where is taged, a changes stage where persist the commits in a file, and then passed to the docker registry on release stage.
